[![Build Status](https://img.shields.io/gitlab/pipeline/Wychmire/wofmc.svg?style=for-the-badge&logo=gitlab)](https://gitlab.com/Wychmire/wofmc/pipelines)

[![`Minecraft Version 1.12.2`](https://img.shields.io/badge/Minecraft%20Version-1.12.2-brightgreen.svg?style=for-the-badge)](https://minecraft.net/download/)
[![`Forge Version 14.23.5.2807`](https://img.shields.io/badge/Forge%20Version-14.23.5.2807-orange.svg?style=for-the-badge)](https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html)

[![`Baubles-1.12-1.5.1`](https://img.shields.io/badge/Baubles%20Version-1.12--1.5.1-ff69b4.svg?style=for-the-badge)](https://minecraft.curseforge.com/projects/baubles) [![`Patchouli-1.0-16`](https://img.shields.io/badge/Patchouli%20Version-1.0--16-ff69b4.svg?style=for-the-badge)](https://minecraft.curseforge.com/projects/patchouli)

![wofmclogo](https://gitlab.com/Wychmire/wofmc/raw/master/src/main/resources/WoFMCLogo.png)


## WoFMC
> An unofficial Minecraft mod about *Wings of Fire*, a book series by **Tui t. Sutherland**.

For Minecraft **1.12.2**
> Forge version [`14.23.2.2768`](https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html) and up.
> 
> [`Baubles version 1.5.2 for Minecraft 1.12.2`](https://minecraft.curseforge.com/projects/baubles) is required.
> 
> [`Patchouli version 1.0-16 for Minecraft 1.12.2`](https://minecraft.curseforge.com/projects/patchouli) is required.

See the [wiki](https://gitlab.com/Wychmire/wofmc/wikis/home) for all added features.

### Downloading

[![Download](https://img.shields.io/badge/Download%20here-1.12.2--0.1.1.0-blue.svg?style=for-the-badge)](https://gitlab.com/Wychmire/wofmc/releases)

**I will *never* put a download link anywhere except for here at [Gitlab](https://gitlab.com/Wychmire/wofmc) and potentially at [CurseForge](https://minecraft.curseforge.com/) if I get everything working.**  
Others are free to upload the mod wherever they want, provided that they include the source code and a copy of the license.

**I will *never* put WoFMC behind Adf.ly or any similar service. If you see a link that uses Adf.ly or a similar service *do not use it*!**

----

#### Want to grab the latest git version?
Keep in mind the git version is much more likely to have bugs!

##### For playing:
> Head to [CI / CD => Pipelines](https://gitlab.com/Wychmire/wofmc/pipelines) and download the artifacts from the latest build by clicking the Cloud Download icon on the right-hand side of the page.
> 
> Then click **Download Java Artifcats**, save the zip file, and unzip it.
> 
> Then navigate to `build/libs` and copy the **non**-sources file to your `.minecraft/mods` folder.
> 
> Launch Minecraft as normal while using the Forge profile and start playing!

##### For developing:

###### Windows
> Download the .zip version,
> 
> Unzip the downloaded .zip file and enter the new folder.
> 
> Shift right-click and open a PowerShell window or Command Prompt window.
> 
> Run `./gradlew setupDecompWorkspace` (drop the `./` if using CMD instead of Powershell).
> 
> If using Eclipse, run `./gradlew eclipse`. If using IntelliJ IDEA, run `./gradlew idea`. Commands will vary for other IDEs.
> 
> Launch your IDE and import the project.
> 
> Start coding!

If you need to build the mod to share the .jar with others,
> Shift right-click and open a PowerShell window or Command Prompt window.
> 
> run `./gradlew build` (drop the `./` if using CMD instead of Powershell).
> 
> Navigate to `build/libs/` and grab the non-sources file (i.e. "WoFMC-${version}.jar", not "WoFMC-${version}-sources.jar").
> 
> Share the .jar file.

###### Linux
> run `git clone https://gitlab.com/Wychmire/wofmc.git`
> 
> `cd wofmc/`.
> 
> run `./gradlew setupDecompWorkspace`.
> 
> If using Eclipse, run `./gradlew eclipse`. If using IntelliJ IDEA, run `./gradlew idea`. Commands will vary for other IDEs.
> 
> Launch your IDE and import the project.
> 
> Start coding!

If you need to build the mod to share the .jar with others,
> run `./gradlew build`.
> 
> Navigate to `build/libs/` and grab the non-sources file (i.e. "WoFMC-${version}.jar", not "WoFMC-${version}-sources.jar").
> 
> Share the .jar file.

----

#### License and Disclaimer

Wings of Fire belongs exclusively to Tui Sutherland, the author. I am simply using her world as a backdrop and an inspiration for modding *Minecraft*

> Wings of Fire Minecraft  
> Copyright (C) 2018  Wychmire
> 
> This program is free software: you can redistribute it and/or modify  
> it under the terms of the GNU General Public License as published by  
> the Free Software Foundation, either version 3 of the License, or  
> (at your option) any later version.

> This program is distributed in the hope that it will be useful,  
> but WITHOUT ANY WARRANTY; without even the implied warranty of  
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
> GNU General Public License for more details.

> You should have received a copy of the GNU General Public License  
> along with this program.  If not, see <http://www.gnu.org/licenses/>.