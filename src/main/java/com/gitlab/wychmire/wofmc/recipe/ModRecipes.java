package com.gitlab.wychmire.wofmc.recipe;

import com.gitlab.wychmire.wofmc.block.ModBlocks;
import com.gitlab.wychmire.wofmc.item.ModItems;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {

	public static void init() {
		ModBlocks.oreCopper.initOreDict();
		ModBlocks.oreTin.initOreDict();

		ModItems.ingotBronze.initOreDict();
		ModItems.ingotCopper.initOreDict();
		ModItems.ingotSilver.initOreDict();
		ModItems.ingotTin.initOreDict();
		
		ModItems.nuggetBronze.initOreDict();
		ModItems.nuggetCopper.initOreDict();
		ModItems.nuggetSilver.initOreDict();
		ModItems.nuggetTin.initOreDict();


		
		//Smelting
		GameRegistry.addSmelting(ModBlocks.oreCopper, new ItemStack(ModItems.ingotCopper), 0.7f);
		GameRegistry.addSmelting(ModBlocks.oreSilver, new ItemStack(ModItems.ingotSilver), 0.7f);
		GameRegistry.addSmelting(ModBlocks.oreTin, new ItemStack(ModItems.ingotTin), 0.7f);
	}
}
