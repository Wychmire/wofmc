package com.gitlab.wychmire.wofmc.tab;

import com.gitlab.wychmire.wofmc.WoFMC;
import com.gitlab.wychmire.wofmc.item.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class WoFMCTabMisc extends CreativeTabs {

	public WoFMCTabMisc() {
		super(WoFMC.modId + "." + "misc");
	}

	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(ModItems.eyeOnyxW);
	}
}
