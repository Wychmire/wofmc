package com.gitlab.wychmire.wofmc.tab;

import com.gitlab.wychmire.wofmc.WoFMC;
import com.gitlab.wychmire.wofmc.item.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class WoFMCTabItems extends CreativeTabs {

	public WoFMCTabItems() {
		super(WoFMC.modId + "." + "item");
	}

	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(ModItems.dragonClaw);
	}
}
