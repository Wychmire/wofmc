package com.gitlab.wychmire.wofmc.tab;

import com.gitlab.wychmire.wofmc.WoFMC;
import com.gitlab.wychmire.wofmc.block.ModBlocks;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class WoFMCTabBlocks extends CreativeTabs {

	public WoFMCTabBlocks() {
		super(WoFMC.modId + "." + "block");
	}

	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(ModBlocks.oreCopper);
	}
}