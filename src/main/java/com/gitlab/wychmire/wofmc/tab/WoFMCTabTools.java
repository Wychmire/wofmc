package com.gitlab.wychmire.wofmc.tab;

import com.gitlab.wychmire.wofmc.WoFMC;
import com.gitlab.wychmire.wofmc.item.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class WoFMCTabTools extends CreativeTabs {

	public WoFMCTabTools() {
		super(WoFMC.modId + "." + "tool");
	}

	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(ModItems.SwordBronze);
	}
}
