package com.gitlab.wychmire.wofmc.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import com.gitlab.wychmire.wofmc.block.silklamp.TileEntitySilkLamp;

public class PacketRequestUpdateSilkLamp implements IMessage {

	private BlockPos pos;
	private int dimension;

	public PacketRequestUpdateSilkLamp(BlockPos pos, int dimension) {
		this.pos = pos;
		this.dimension = dimension;
	}

	public PacketRequestUpdateSilkLamp(TileEntitySilkLamp te) {
		this(te.getPos(), te.getWorld().provider.getDimension());
	}

	public PacketRequestUpdateSilkLamp() {
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeLong(pos.toLong());
		buf.writeInt(dimension);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		pos = BlockPos.fromLong(buf.readLong());
		dimension = buf.readInt();
	}

	public static class Handler implements IMessageHandler<PacketRequestUpdateSilkLamp, PacketUpdateSilkLamp> {

		@Override
		public PacketUpdateSilkLamp onMessage(PacketRequestUpdateSilkLamp message, MessageContext ctx) {
			World world = FMLCommonHandler.instance().getMinecraftServerInstance().getWorld(message.dimension);
			TileEntitySilkLamp te = (TileEntitySilkLamp)world.getTileEntity(message.pos);
			if (te != null) {
				return new PacketUpdateSilkLamp(te);
			} else {
				return null;
			}
		}

	}
}