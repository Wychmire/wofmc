package com.gitlab.wychmire.wofmc.handler;

import com.gitlab.wychmire.wofmc.block.silklamp.ContainerSilkLamp;
import com.gitlab.wychmire.wofmc.block.silklamp.GuiSilkLamp;
import com.gitlab.wychmire.wofmc.block.silklamp.TileEntitySilkLamp;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class ModGuiHandler implements IGuiHandler {

	public static final int silkLamp = 0;

	@Override
	public Container getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch (ID) {
			case silkLamp:
				return new ContainerSilkLamp(player.inventory, (TileEntitySilkLamp)world.getTileEntity(new BlockPos(x, y, z)));
			default:
				return null;
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch (ID) {
			case silkLamp:
				return new GuiSilkLamp(getServerGuiElement(ID, player, world, x, y, z), player.inventory);
			default:
				return null;
		}
	}
}
