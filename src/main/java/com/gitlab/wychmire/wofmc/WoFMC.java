package com.gitlab.wychmire.wofmc;

import com.gitlab.wychmire.wofmc.block.ModBlocks;
import com.gitlab.wychmire.wofmc.handler.ModGuiHandler;
import com.gitlab.wychmire.wofmc.item.ModItems;
import com.gitlab.wychmire.wofmc.network.PacketRequestUpdateSilkLamp;
import com.gitlab.wychmire.wofmc.network.PacketUpdateSilkLamp;
import com.gitlab.wychmire.wofmc.proxy.CommonProxy;
import com.gitlab.wychmire.wofmc.recipe.ModRecipes;
import com.gitlab.wychmire.wofmc.tab.WoFMCTabBlocks;
import com.gitlab.wychmire.wofmc.tab.WoFMCTabItems;
import com.gitlab.wychmire.wofmc.tab.WoFMCTabMisc;
import com.gitlab.wychmire.wofmc.tab.WoFMCTabTools;
import com.gitlab.wychmire.wofmc.world.ModWorldGen;

import net.minecraft.block.Block;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = WoFMC.modId, name = WoFMC.name, version = WoFMC.version, updateJSON = WoFMC.updateURL)
public class WoFMC {

	public static final String modId = "wofmc";
	public static final String name = "Wings of Fire Minecraft";
	public static final String version = "0.1.1.0";
	public static final String updateURL = "https://www.jasonbase.com/things/y7ME.json";
	
	@Mod.Instance(modId)
	public static WoFMC instance;

	@SidedProxy(serverSide = "com.gitlab.wychmire.wofmc.CommonProxy", clientSide = "com.gitlab.wychmire.wofmc.proxy.ClientProxy")
	public static CommonProxy proxy;

	// Creative Tabs
	public static final WoFMCTabItems WoFMCTabItems = new WoFMCTabItems();
	public static final WoFMCTabBlocks WoFMCTabBlocks = new WoFMCTabBlocks();
	public static final WoFMCTabTools WoFMCTabTools = new WoFMCTabTools();
	public static final WoFMCTabMisc WoFMCTabMisc = new WoFMCTabMisc();
	// Tool and Armor Materials
	public static final Item.ToolMaterial bronzeToolMaterial = EnumHelper.addToolMaterial("BRONZE", 2, 500, 6, 2, 14);
	public static final Item.ToolMaterial boneToolMaterial = EnumHelper.addToolMaterial("DBONE", 2, 600, 8, 3, 16);
	public static final ItemArmor.ArmorMaterial BronzeArmorMaterial = EnumHelper.addArmorMaterial("BRONZE", modId + ":bronze", 15, new int[]{1, 4, 5, 1}, 9, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0.0F);
	public static final ItemArmor.ArmorMaterial BoneArmorMaterial = EnumHelper.addArmorMaterial("DBONE", modId + ":dbone", 15, new int[]{3, 6, 6, 3}, 9, SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, 0.0F);

	public static SimpleNetworkWrapper network;

	public void registerItemRenderer(Item item, int meta, String id) {

	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {

	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {

	}

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		System.out.println(WoFMC.name + " has started loading!");
		GameRegistry.registerWorldGenerator(new ModWorldGen(), 3);
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new ModGuiHandler());
		proxy.registerRenderers();

		network = NetworkRegistry.INSTANCE.newSimpleChannel(modId);
		network.registerMessage(new PacketUpdateSilkLamp.Handler(), PacketUpdateSilkLamp.class, 0, Side.CLIENT);
		network.registerMessage(new PacketRequestUpdateSilkLamp.Handler(), PacketRequestUpdateSilkLamp.class, 1, Side.SERVER);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		ModRecipes.init();
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {

	}

	@Mod.EventBusSubscriber
	public static class RegsitrationHandler {

		@SubscribeEvent
		public static void registerItems(RegistryEvent.Register<Item> event) {
			ModItems.register(event.getRegistry());
			ModBlocks.registerItemBlocks(event.getRegistry());
		}

		@SubscribeEvent
		public static void registerBlocks(RegistryEvent.Register<Block> event) {
			ModBlocks.register(event.getRegistry());
		}

		@SubscribeEvent
		public static void registerModels(ModelRegistryEvent event) {
			ModItems.registerModels();
			ModBlocks.registerModels();
		}
	}
}