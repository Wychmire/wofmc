package com.gitlab.wychmire.wofmc.block;

import com.gitlab.wychmire.wofmc.block.egg.BlockBloodRedEgg;
import com.gitlab.wychmire.wofmc.block.egg.BlockEgg;
import com.gitlab.wychmire.wofmc.block.egg.BlockFireSilkEgg;
import com.gitlab.wychmire.wofmc.block.egg.BlockMoonEgg;
import com.gitlab.wychmire.wofmc.block.silklamp.BlockSilkLamp;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.registries.IForgeRegistry;

public class ModBlocks {

	// Ores
	public static BlockOre oreCopper = new BlockOre("ore_copper", "oreCopper");
	public static BlockOre oreSilver = new BlockOre("ore_silver", "oreSilver");
	public static BlockOre oreTin = new BlockOre("ore_tin", "oreTin");
	
	// Eggs
		// Arc 1 Eggs
		public static BlockEgg eggIceWing = new BlockEgg("egg_icewing", "eggIce");
		public static BlockEgg eggMudWing = new BlockEgg("egg_mudwing", "eggMud");
		public static BlockEgg eggNightWing = new BlockEgg("egg_nightwing", "eggNight");
		public static BlockEgg eggRainWing = new BlockEgg("egg_rainwing", "eggRain");
		public static BlockEgg eggSandWing = new BlockEgg("egg_sandwing", "eggSand");
		public static BlockEgg eggSeaWing = new BlockEgg("egg_seawing", "eggSea");
		public static BlockEgg eggSkyWing = new BlockEgg("egg_skywing", "eggSky");
		// Arc 1 Special Eggs
		public static BlockBloodRedEgg eggBloodred = new BlockBloodRedEgg("egg_bloodred", "eggBRMW");
		public static BlockMoonEgg eggMoontouched = new BlockMoonEgg("egg_moontouched", "eggMTNW");
		// Arc 3 Eggs
		public static BlockEgg eggHiveWing = new BlockEgg("egg_hivewing", "eggIce");
		public static BlockEgg eggLeafWing = new BlockEgg("egg_leafwing", "eggMud");
		public static BlockEgg eggSilkWing = new BlockEgg("egg_silkwing", "eggNight");
		// Arc 3 Special Eggs
		public static BlockFireSilkEgg eggFiresilk = new BlockFireSilkEgg("egg_firesilk", "eggFSSW");

	// Other Bricks
	public static BlockSilkLamp silkLamp = new BlockSilkLamp(false);


	public static void register(IForgeRegistry<Block> registry) {
		registry.registerAll (
			oreCopper,
			oreSilver,
			oreTin,
			// Eggs
				// Arc 1 Eggs
				eggIceWing,
				eggMudWing,
				eggNightWing,
				eggRainWing,
				eggSandWing,
				eggSeaWing,
				eggSkyWing,
				// Arc 1 Special Eggs
				eggBloodred,
				eggMoontouched,
				// Arc 3 Eggs
				eggHiveWing,
				eggLeafWing,
				eggSilkWing,
				// Arc 3 Special Eggs
				eggFiresilk,
			// Other Bricks
			silkLamp
		);
	}

	public static void registerItemBlocks(IForgeRegistry<Item> registry) {
		registry.registerAll (
			oreCopper.createItemBlock(),
			oreTin.createItemBlock(),
			oreSilver.createItemBlock(),
			// Eggs
				// Arc 1 Eggs
				eggIceWing.createItemBlock(),
				eggMudWing.createItemBlock(),
				eggNightWing.createItemBlock(),
				eggRainWing.createItemBlock(),
				eggSandWing.createItemBlock(),
				eggSeaWing.createItemBlock(),
				eggSkyWing.createItemBlock(),
				// Arc 1 Special Eggs
				eggBloodred.createItemBlock(),
				eggMoontouched.createItemBlock(),
				// Arc 3 Eggs
				eggHiveWing.createItemBlock(),
				eggLeafWing.createItemBlock(),
				eggSilkWing.createItemBlock(),
				// Arc 3 Special Eggs
				eggFiresilk.createItemBlock(),
			// Other Bricks
			silkLamp.createItemBlock()
		);
	}

	public static void registerModels() {
		oreCopper.registerItemModel(Item.getItemFromBlock(oreCopper));
		oreSilver.registerItemModel(Item.getItemFromBlock(oreSilver));
		oreTin.registerItemModel(Item.getItemFromBlock(oreTin));
		// Eggs
			// Arc 1 Eggs
			eggIceWing.registerItemModel(Item.getItemFromBlock(eggIceWing));
			eggMudWing.registerItemModel(Item.getItemFromBlock(eggMudWing));
			eggNightWing.registerItemModel(Item.getItemFromBlock(eggNightWing));
			eggRainWing.registerItemModel(Item.getItemFromBlock(eggRainWing));
			eggSandWing.registerItemModel(Item.getItemFromBlock(eggSandWing));
			eggSeaWing.registerItemModel(Item.getItemFromBlock(eggSeaWing));
			eggSkyWing.registerItemModel(Item.getItemFromBlock(eggSkyWing));
			// Arc 1 Special Eggs
			eggBloodred.registerItemModel(Item.getItemFromBlock(eggBloodred));
			eggMoontouched.registerItemModel(Item.getItemFromBlock(eggMoontouched));
			// Arc 3 Eggs
			eggHiveWing.registerItemModel(Item.getItemFromBlock(eggHiveWing));
			eggLeafWing.registerItemModel(Item.getItemFromBlock(eggLeafWing));
			eggSilkWing.registerItemModel(Item.getItemFromBlock(eggSilkWing));
			// Arc 3 Special Eggs
			eggFiresilk.registerItemModel(Item.getItemFromBlock(eggFiresilk));
		// Other Bricks
		silkLamp.registerItemModel(Item.getItemFromBlock(silkLamp));
	}
}