package com.gitlab.wychmire.wofmc.block.egg;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.block.BlockFalling;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockMoonEgg extends BlockFalling {

protected String name;
	
	public BlockMoonEgg(String name, String eggName) {
		super();
		this.name = name;
		setUnlocalizedName(name);
		setRegistryName(name);
		setHardness(0.5f);
		setResistance(40f);
		setLightLevel(0.2f);
		setCreativeTab(WoFMC.WoFMCTabBlocks);
	}
	
	@Override
	@Deprecated
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}
	
	@Override
	@Deprecated
	public boolean isFullCube(IBlockState state) {
		return false;
	}
	
	@Override
	public BlockMoonEgg setCreativeTab(CreativeTabs tab) {
		super.setCreativeTab(tab);
		return this;
	}
	
	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(item, 0, name);
	}

	public Item createItemBlock() {
		return new ItemBlock(this).setRegistryName(getRegistryName());
	}
}