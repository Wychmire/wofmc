package com.gitlab.wychmire.wofmc.item;

import com.gitlab.wychmire.wofmc.WoFMC;
import com.gitlab.wychmire.wofmc.item.special.ItemDarkstalkerScroll;
import com.gitlab.wychmire.wofmc.item.special.ItemDiplomacyBracelet;
import com.gitlab.wychmire.wofmc.item.special.ItemDiplomacyBraceletWearable;
import com.gitlab.wychmire.wofmc.item.special.ItemDreamVistor;
import com.gitlab.wychmire.wofmc.item.special.ItemEyeOnyx;
import com.gitlab.wychmire.wofmc.item.special.ItemEyeOnyxWearable;
import com.gitlab.wychmire.wofmc.item.special.ItemObsidianMirror;
import com.gitlab.wychmire.wofmc.item.special.ItemSoulReader;
import com.gitlab.wychmire.wofmc.item.special.ItemTurtleSeaweed;
import com.gitlab.wychmire.wofmc.item.tool.ItemBronzeAxe;
import com.gitlab.wychmire.wofmc.item.tool.ItemBronzeHoe;
import com.gitlab.wychmire.wofmc.item.tool.ItemBronzePickaxe;
import com.gitlab.wychmire.wofmc.item.tool.ItemBronzeShovel;
import com.gitlab.wychmire.wofmc.item.tool.ItemBronzeSword;
import com.gitlab.wychmire.wofmc.item.tool.ItemDBoneAxe;
import com.gitlab.wychmire.wofmc.item.tool.ItemDBoneHoe;
import com.gitlab.wychmire.wofmc.item.tool.ItemDBonePickaxe;
import com.gitlab.wychmire.wofmc.item.tool.ItemDBoneShovel;
import com.gitlab.wychmire.wofmc.item.tool.ItemDBoneSword;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.registries.IForgeRegistry;

public class ModItems {
	// ==============================
	// Dragon Armor + Items + Tools
	// ==============================

	// Dragon Bone Armor
	public static ItemArmor HelmetDBone = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BoneArmorMaterial, EntityEquipmentSlot.HEAD, "helmet_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemArmor ChestDBone = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BoneArmorMaterial, EntityEquipmentSlot.CHEST, "chest_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemArmor LegsDBone = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BoneArmorMaterial, EntityEquipmentSlot.LEGS, "legs_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemArmor BootsDBone = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BoneArmorMaterial, EntityEquipmentSlot.FEET, "feet_dbone").setCreativeTab(WoFMC.WoFMCTabTools);

	// Dragon Items
	public static ItemBase dragonBone = new ItemBase("dragon_bone");
	public static ItemBase dragonShard = new ItemBase("dragon_shard");
	public static ItemBase dragonClaw = new ItemBase("dragon_claw");
	public static ItemBase dragonTooth = new ItemBase("dragon_tooth");
	// Dragon scales
		// Arc 1 scales
		public static ItemBase scaleIcewing = new ItemBase("scale_icewing");
		public static ItemBase scaleMudwing = new ItemBase("scale_mudwing");
		public static ItemBase scaleNightwing = new ItemBase("scale_nightwing");
		public static ItemBase scaleRainwing = new ItemBase("scale_rainwing");
		public static ItemBase scaleSandwing = new ItemBase("scale_sandwing");
		public static ItemBase scaleSeawing = new ItemBase("scale_seawing");
		public static ItemBase scaleSkywing = new ItemBase("scale_skywing");
		//Arc 3 scales
		public static ItemBase scaleHivewing = new ItemBase("scale_hivewing");
		public static ItemBase scaleLeafwing = new ItemBase("scale_leafwing");
		public static ItemBase scaleSilkwing = new ItemBase("scale_silkwing");

	// Dragon Bone Tools
	public static ItemDBoneAxe AxeDBone = (ItemDBoneAxe) new ItemDBoneAxe(WoFMC.boneToolMaterial, "axe_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemDBoneHoe HoeDBone = (ItemDBoneHoe) new ItemDBoneHoe(WoFMC.boneToolMaterial, "hoe_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemDBonePickaxe PickaxeDBone = (ItemDBonePickaxe) new ItemDBonePickaxe(WoFMC.boneToolMaterial, "pickaxe_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemDBoneShovel ShovelDBone = (ItemDBoneShovel) new ItemDBoneShovel(WoFMC.boneToolMaterial, "shovel_dbone").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemDBoneSword SwordDBone = (ItemDBoneSword) new ItemDBoneSword(WoFMC.boneToolMaterial, "sword_dbone").setCreativeTab(WoFMC.WoFMCTabTools);

	// =============================
	// Metal Armor + Items + Tools
	// =============================

	// Metal Armor
	public static ItemArmor HelmetBronze = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BronzeArmorMaterial, EntityEquipmentSlot.HEAD, "helmet_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemArmor ChestBronze = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BronzeArmorMaterial, EntityEquipmentSlot.CHEST, "chest_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemArmor LegsBronze = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BronzeArmorMaterial, EntityEquipmentSlot.LEGS, "legs_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemArmor BootsBronze = (ItemArmor) new com.gitlab.wychmire.wofmc.item.ItemArmor(WoFMC.BronzeArmorMaterial, EntityEquipmentSlot.FEET, "feet_bronze").setCreativeTab(WoFMC.WoFMCTabTools);

	// Metal Items
	public static ItemOre ingotBronze = (ItemOre) new ItemOre("ingot_bronze", "ingotBronze");
	public static ItemOre ingotCopper = (ItemOre) new ItemOre("ingot_copper", "ingotCopper");
	public static ItemOre ingotSilver = (ItemOre) new ItemOre("ingot_silver", "ingotSilver");
	public static ItemOre ingotTin = (ItemOre) new ItemOre("ingot_tin", "ingotTin");
	public static ItemOre nuggetBronze = (ItemOre) new ItemOre("nugget_bronze", "nuggetBronze");
	public static ItemOre nuggetCopper = (ItemOre) new ItemOre("nugget_copper", "nuggetCopper");
	public static ItemOre nuggetSilver = (ItemOre) new ItemOre("nugget_silver", "nuggetSilver");
	public static ItemOre nuggetTin = (ItemOre) new ItemOre("nugget_tin", "nuggetTin");

	// Metal Tools
	public static ItemBronzeAxe AxeBronze = (ItemBronzeAxe) new ItemBronzeAxe(WoFMC.bronzeToolMaterial, "axe_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemBronzeHoe HoeBronze = (ItemBronzeHoe) new ItemBronzeHoe(WoFMC.bronzeToolMaterial, "hoe_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemBronzePickaxe PickaxeBronze = (ItemBronzePickaxe) new ItemBronzePickaxe(WoFMC.bronzeToolMaterial, "pickaxe_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemBronzeShovel ShovelBronze = (ItemBronzeShovel) new ItemBronzeShovel(WoFMC.bronzeToolMaterial, "shovel_bronze").setCreativeTab(WoFMC.WoFMCTabTools);
	public static ItemBronzeSword SwordBronze = (ItemBronzeSword) new ItemBronzeSword(WoFMC.bronzeToolMaterial, "sword_bronze").setCreativeTab(WoFMC.WoFMCTabTools);

	// =============================
	// Miscellaneous
	// =============================
	public static ItemDarkstalkerScroll darkstalkerScroll = (ItemDarkstalkerScroll) new ItemDarkstalkerScroll("darkstalker_scroll").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemDiplomacyBracelet diplomacyBracelet = (ItemDiplomacyBracelet) new ItemDiplomacyBracelet("diplomacy_bracelet").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemDiplomacyBraceletWearable diplomacyBraceletW = (ItemDiplomacyBraceletWearable) new ItemDiplomacyBraceletWearable("diplomacy_braceletw").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemDreamVistor dreamVistor = (ItemDreamVistor) new ItemDreamVistor("dream_vistor").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemEyeOnyx eyeOnyx = (ItemEyeOnyx) new ItemEyeOnyx("eye_onyx").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemEyeOnyxWearable eyeOnyxW = (ItemEyeOnyxWearable) new ItemEyeOnyxWearable("eye_onyxw").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemObsidianMirror obsidianMirror = (ItemObsidianMirror) new ItemObsidianMirror("obsidian_mirror").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemSoulReader soulReader = (ItemSoulReader) new ItemSoulReader("soul_reader").setCreativeTab(WoFMC.WoFMCTabMisc);
	public static ItemTurtleSeaweed turtleSeaweed = (ItemTurtleSeaweed) new ItemTurtleSeaweed("turtle_Seaweed").setCreativeTab(WoFMC.WoFMCTabMisc);

	public static void register(IForgeRegistry<Item> registry) {
		registry.registerAll (
			// Dragon Items
			dragonBone,
			dragonShard,
			dragonClaw,
			dragonTooth,
				// Arc 1 scales
				scaleIcewing,
				scaleMudwing,
				scaleNightwing,
				scaleRainwing,
				scaleSandwing,
				scaleSeawing,
				scaleSkywing,
				// Arc 3 scales
				scaleHivewing,
				scaleLeafwing,
				scaleSilkwing,
			// Dragon Armor
			HelmetDBone,
			ChestDBone,
			LegsDBone,
			BootsDBone,
			// Dragon Tools
			AxeDBone,
			HoeDBone,
			PickaxeDBone,
			ShovelDBone,
			SwordDBone,
			// Metal Armor
			HelmetBronze,
			ChestBronze,
			LegsBronze,
			BootsBronze,
			// Metal Items
			ingotBronze,
			ingotCopper,
			ingotSilver,
			ingotTin,
			nuggetBronze,
			nuggetCopper,
			nuggetSilver,
			nuggetTin,
			// Metal Tools
			AxeBronze,
			HoeBronze,
			PickaxeBronze,
			ShovelBronze,
			SwordBronze,
			// Misc
			darkstalkerScroll,
			diplomacyBracelet,
			diplomacyBraceletW,
			dreamVistor,
			eyeOnyx,
			eyeOnyxW,
			obsidianMirror,
			soulReader,
			turtleSeaweed
		);
	}

	public static void registerModels() {
		// Dragon Armor
		((com.gitlab.wychmire.wofmc.item.ItemArmor) HelmetDBone).registerItemModel(HelmetDBone);
		((com.gitlab.wychmire.wofmc.item.ItemArmor) ChestDBone).registerItemModel(ChestDBone);
		((com.gitlab.wychmire.wofmc.item.ItemArmor) LegsDBone).registerItemModel(LegsDBone);
		((com.gitlab.wychmire.wofmc.item.ItemArmor) BootsDBone).registerItemModel(BootsDBone);
		// Dragon Items
		dragonBone.registerItemModel();
		dragonShard.registerItemModel();
		dragonClaw.registerItemModel();
		dragonTooth.registerItemModel();
			// Arc 1 scales
			scaleIcewing.registerItemModel();
			scaleMudwing.registerItemModel();
			scaleNightwing.registerItemModel();
			scaleRainwing.registerItemModel();
			scaleSandwing.registerItemModel();
			scaleSeawing.registerItemModel();
			scaleSkywing.registerItemModel();
			// Arc 3 scales
			scaleHivewing.registerItemModel();
			scaleLeafwing.registerItemModel();
			scaleSilkwing.registerItemModel();
		// Dragon Tools
		AxeDBone.registerItemModel(AxeDBone);
		HoeDBone.registerItemModel(HoeDBone);
		PickaxeDBone.registerItemModel(PickaxeDBone);
		ShovelDBone.registerItemModel(ShovelDBone);
		SwordDBone.registerItemModel(SwordDBone);
		// Metal Armor
		((com.gitlab.wychmire.wofmc.item.ItemArmor) HelmetBronze).registerItemModel(HelmetBronze);
		((com.gitlab.wychmire.wofmc.item.ItemArmor) ChestBronze).registerItemModel(ChestBronze);
		((com.gitlab.wychmire.wofmc.item.ItemArmor) LegsBronze).registerItemModel(LegsBronze);
		((com.gitlab.wychmire.wofmc.item.ItemArmor) BootsBronze).registerItemModel(BootsBronze);
		// Metal Items
		ingotBronze.registerItemModel();
		ingotCopper.registerItemModel();
		ingotSilver.registerItemModel();
		ingotTin.registerItemModel();
		nuggetBronze.registerItemModel();
		nuggetCopper.registerItemModel();
		nuggetSilver.registerItemModel();
		nuggetTin.registerItemModel();
		// Metal Tools
		AxeBronze.registerItemModel(AxeBronze);
		HoeBronze.registerItemModel(HoeBronze);
		PickaxeBronze.registerItemModel(PickaxeBronze);
		ShovelBronze.registerItemModel(ShovelBronze);
		SwordBronze.registerItemModel(SwordBronze);
		// Misc
		darkstalkerScroll.registerItemModel(darkstalkerScroll);
		diplomacyBracelet.registerItemModel(diplomacyBracelet);
		diplomacyBraceletW.registerItemModel(diplomacyBraceletW);
		dreamVistor.registerItemModel(dreamVistor);
		eyeOnyx.registerItemModel(eyeOnyx);
		eyeOnyxW.registerItemModel(eyeOnyxW);
		obsidianMirror.registerItemModel(obsidianMirror);
		soulReader.registerItemModel(soulReader);
		turtleSeaweed.registerItemModel(turtleSeaweed);
	}
}