package com.gitlab.wychmire.wofmc.item.special;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemSilkThread extends Item {
	
	private String name;

	public ItemSilkThread(String name) {
		super();
		setRegistryName(name);
		setUnlocalizedName(name);
		this.setMaxStackSize(16);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}
