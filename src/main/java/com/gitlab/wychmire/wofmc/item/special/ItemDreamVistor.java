package com.gitlab.wychmire.wofmc.item.special;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemDreamVistor extends Item {
	
	private String name;

	public ItemDreamVistor(String name) {
		setRegistryName(name);
		setUnlocalizedName(name);
		this.setMaxStackSize(3);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}
