package com.gitlab.wychmire.wofmc.item.special;

import com.gitlab.wychmire.wofmc.WoFMC;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;

public class ItemEyeOnyxWearable extends ItemSpecial implements IBauble {
	
	private String name;

	public static final Item AMULET = null;

	public ItemEyeOnyxWearable(String name) {
		super();
		setMaxDamage(10);
		setRegistryName(name);
		setUnlocalizedName(name);
		this.setMaxStackSize(1);
		this.name = name;
	}

	@Override
	public BaubleType getBaubleType(ItemStack itemstack) {
		return BaubleType.AMULET;
	}

	@Override
	public EnumRarity getRarity(ItemStack par1ItemStack) {
		return EnumRarity.EPIC;
	}

	@Override
	public void onWornTick(ItemStack itemstack, EntityLivingBase player) {
		if (itemstack.getItemDamage()==0 && player.ticksExisted%39==0) {
			player.addPotionEffect(new PotionEffect(MobEffects.STRENGTH,40,0,true,true));
			player.addPotionEffect(new PotionEffect(MobEffects.RESISTANCE,40,0,true,true));
			player.addPotionEffect(new PotionEffect(MobEffects.LEVITATION,40,-5,true,true));
			player.addPotionEffect(new PotionEffect(MobEffects.FIRE_RESISTANCE,40,0,true,true));
			player.fallDistance = 0;
		}
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}

	@Override
	public void onEquipped(ItemStack itemstack, EntityLivingBase player) {
		player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, .75F, 1.9f);
	}

	@Override
	public void onUnequipped(ItemStack itemstack, EntityLivingBase player) {
		player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, .75F, 2f);
	}
}