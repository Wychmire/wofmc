package com.gitlab.wychmire.wofmc.item;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBase extends Item {

	protected String name;

	public ItemBase(String name) {
		this.name = name;
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(WoFMC.WoFMCTabItems);
	}

	public void registerItemModel() {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}

	@Override
	public ItemBase setCreativeTab(CreativeTabs tab) {
		super.setCreativeTab(tab);
		return this;
	}

	public void initOreDict() {

	}
}