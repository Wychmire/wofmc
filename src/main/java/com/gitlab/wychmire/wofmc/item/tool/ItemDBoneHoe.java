package com.gitlab.wychmire.wofmc.item.tool;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemDBoneHoe
extends net.minecraft.item.ItemHoe {
	
	private String name;

	public ItemDBoneHoe(ToolMaterial material, String name) {
		super(material);
		setRegistryName(name);
		setUnlocalizedName(name);
		setMaxDamage(1500);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}
