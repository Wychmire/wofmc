package com.gitlab.wychmire.wofmc.item.special;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemDiplomacyBracelet extends Item {
	
	private String name;

	public ItemDiplomacyBracelet(String name) {
		setRegistryName(name);
		setUnlocalizedName(name);
		this.setMaxStackSize(1);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}
