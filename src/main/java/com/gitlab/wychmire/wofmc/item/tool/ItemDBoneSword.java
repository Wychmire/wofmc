package com.gitlab.wychmire.wofmc.item.tool;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemDBoneSword
extends net.minecraft.item.ItemSword {
	
	private String name;

	public ItemDBoneSword(ToolMaterial material, String name) {
		super(material);
		setRegistryName(name);
		setUnlocalizedName(name);
		setMaxDamage(1500);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}