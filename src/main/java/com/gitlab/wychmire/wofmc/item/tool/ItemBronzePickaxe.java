package com.gitlab.wychmire.wofmc.item.tool;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemBronzePickaxe
extends net.minecraft.item.ItemPickaxe {
	
	private String name;

	public ItemBronzePickaxe(ToolMaterial material, String name) {
		super(material);
		setRegistryName(name);
		setUnlocalizedName(name);
		setMaxDamage(141);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}