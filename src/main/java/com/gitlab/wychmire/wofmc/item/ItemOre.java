package com.gitlab.wychmire.wofmc.item;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraftforge.oredict.OreDictionary;

public class ItemOre extends ItemBase {
	private String oreName;

	public ItemOre(String name, String oreName) {
		super(name);
		this.oreName = oreName;
		setCreativeTab(WoFMC.WoFMCTabItems);
	}

	public void initOreDict() {
		OreDictionary.registerOre(oreName, this);
	}
}