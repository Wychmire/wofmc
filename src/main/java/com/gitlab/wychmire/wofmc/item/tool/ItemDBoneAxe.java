package com.gitlab.wychmire.wofmc.item.tool;

import com.gitlab.wychmire.wofmc.WoFMC;

import net.minecraft.item.Item;

public class ItemDBoneAxe
extends net.minecraft.item.ItemAxe {
	
	private String name;

	public ItemDBoneAxe(ToolMaterial material, String name) {
		super(material, 8f, -3.1f);
		setRegistryName(name);
		setUnlocalizedName(name);
		setMaxDamage(1500);
		this.name = name;
	}

	public void registerItemModel(Item item) {
		WoFMC.proxy.registerItemRenderer(this, 0, name);
	}
}